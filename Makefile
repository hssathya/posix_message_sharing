#CC := arm-xilinx-linux-gnueabi-gcc
#CC := arm-linux-gnueabihf-gcc
CROSS_COMPILE := arm-linux-gnueabihf-
CC := $(CROSS_COMPILE)gcc
ifeq ($(CROSS_COMPILE), arm-linux-gnueabihf-)
CFLAGS := -Wall
else
CFLAGS := -Wall -DENABLE_X86
endif

ifeq ($(OS),Windows_NT)
   SHELL = cmd.exe
   RM = del /Q $(DEPDIR)\*.d $(OBJDIR)\*.o $(subst /,\\,$(OUTPUT))
else
   RM = rm -f $(DEPDIR)/*.d $(OBJDIR)/*.o *~ $(OUTPUT)
endif

OUTDIR := out
DEPDIR := $(OUTDIR)/dep
OBJDIR := $(OUTDIR)/obj
#Sathya
#ifeq ($(CROSS_COMPILE), arm-linux-gnueabihf-)
#LIBMODBUSDIR := /home/usr/share/Servo/Alpha5Motor/libModBus_RTU_REQ/libmodbus/src/.libs
#else
#LIBMODBUSDIR := /home/usr/share/libmod/libmodbus-3.0.6/src/.libs
#endif
OUTPUT := $(OUTDIR)/inter_task

LOCAL_SRC_DIRS := .

LOCAL_INC_DIRS := .
ifeq ($(CROSS_COMPILE), arm-linux-gnueabihf-)
LOCAL_INC_DIRS += /home/sathya/inter_task/inc
else
LOCAL_INC_DIRS += /home/sathya/inter_task/inc
endif

SRC := $(foreach sdir,$(LOCAL_SRC_DIRS),$(wildcard $(sdir)/*.c))
INC := $(foreach idir,$(LOCAL_INC_DIRS),-I$(idir))
OBJ := $(addprefix $(OBJDIR)/,$(notdir $(SRC:.c=.o)))
DEP := $(addprefix $(DEPDIR)/,$(notdir $(OBJ:.o=.d)))
LIBS := pthread rt

VPATH = $(LOCAL_SRC_DIRS)

ifneq "$(findstring clean,$(MAKECMDGOALS))" "clean"
-include $(DEP)
endif

.PHONY: all clean dirtree
.DEFAULT_GOAL := all

all: $(OUTPUT)

$(OUTPUT): $(OBJ)
	$(CC) $^ $(addprefix -l, $(LIBS)) -o $@

$(DEPDIR)/%.d: %.c
	$(CC) -MM -MF $@ -MP -MT "$(OBJDIR)/$*.o $@" $(CFLAGS) $(INC) $<

$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

$(OBJ) $(DEP): | dirtree

dirtree:
ifeq ($(OS),Windows_NT)
	if not exist $(subst /,\\,$(DEPDIR)) mkdir $(subst /,\\,$(DEPDIR))
	if not exist $(subst /,\\,$(OBJDIR)) mkdir $(subst /,\\,$(OBJDIR))
	if not exist $(subst /,\\,$(OUTDIR)) mkdir $(subst /,\\,$(OUTDIR))
else
	mkdir -p $(DEPDIR) $(OUTDIR) $(OBJDIR)
endif

clean:
	$(RM)
